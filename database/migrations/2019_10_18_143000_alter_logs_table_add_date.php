<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLogsTableAddDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function (Blueprint $table) {
            $table->timestamp('date')->nullable()->after('author_id');
            $table->index('date');
        });
        \DB::statement('UPDATE logs SET date = created_at');
        foreach (\Hermit\Logs\Logger::LOG_LEVEL as $type_name => $lowercase) {
            if (!\Hermit\Logs\LogType::where('name', $type_name)->first()) {
                \Hermit\Logs\LogType::create([
                    'name' => $type_name,
                    'priority' => 1
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('logs', 'date')) {
            Schema::table('logs', function (Blueprint $table) {
                $table->dropColumn('date');
            });
        }
        foreach (\Hermit\Logs\Logger::LOG_LEVEL as $type_name => $lowercase) {
            if (!\Hermit\Logs\Log::where('type', $type_name)->first()) {
                \Hermit\Logs\LogType::where('name', $type_name)->delete();
            }
        }
    }
}
