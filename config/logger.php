<?php

return [

    'enabled' => env('LOGS_ENABLED', true),

    /*
    |--------------------------------------------------------------------------
    | Logs destination
    |--------------------------------------------------------------------------
    |
    | This option defines the destination where logs will be saved.
    | Available options:
    | - file - save to file in storage/logs; name: logger-YYYY-MM-DD.log
    | - db - save to logs table in database
    |
    */

    'destination' => env('LOG_DESTINATION', 'file'),

    /*
    |--------------------------------------------------------------------------
    | Log files path
    |--------------------------------------------------------------------------
    |
    | This option defines directory where file logs will be saved.
    | Default path: logs refers to storage/logs
    | This string is used in storage_path() method.
    |
    */

    'file_path' => env('LOG_FILE_PATH', 'logs'),

    /*
    |--------------------------------------------------------------------------
    | Log files lifespan
    |--------------------------------------------------------------------------
    |
    | This option defines lifespan of created log files.
    | If file is older than specified amount of days it will be deleted.
    | Requires destination to be set on file and laravel scheduler must be active.
    | Command handling files will run every day at 1:00 AM
    |
    */

    'max_number_of_files' => env('LOG_MAX_FILES', 14),

    /*
    |--------------------------------------------------------------------------
    | Logger save method
    |--------------------------------------------------------------------------
    |
    | This option defines the the way how logs will be saved to destination.
    | Available options:
    | - direct - logs will be saved the moment they occur
    | - redis - logs will be saved to cache using predis/predis package and then put into specified destination
    |
    | If you want to use redis method laravel scheduler must be active. Command handling redis list will be run on each cron cycle.
    |
    | Each log of emergency, alert, critical and error level will be saved directly.
    |
    */
    'save_method' => env('LOG_SAVE_METHOD', 'direct'),

    /*
    |--------------------------------------------------------------------------
    | Batch of logs saved at a time when using redis method
    |--------------------------------------------------------------------------
    |
    | This option defines the number of logs that will be taken from redis list to save into destination.
    |
    */
    'redis_save_batch' => env('LOGS_REDIS_BATCH', 100),

    /*
    |--------------------------------------------------------------------------
    | Automatic database logs archive
    |--------------------------------------------------------------------------
    |
    | This option defines if logs stored in database should be archived.
    | Requires laravel scheduler to be active.
    | Logs are archived at the beginning of each month.
    |
    */
    'automatic_db_archive' => env('LOGS_AUTOMATIC_ARCHIVE', true),

    /*
    |--------------------------------------------------------------------------
    | Database logs lifespan
    |--------------------------------------------------------------------------
    |
    | Logs older than given number of months will be archived if automatic archive is set to true.
    | Logs are archived on the beginning of each month.
    |
    */
    'db_lifespan' => env('LOGS_LIFESPAN', 3),

    /*
    |--------------------------------------------------------------------------
    | Logs archive batch
    |--------------------------------------------------------------------------
    |
    | This option defines the number of logs that will be put in one archive record.
    |
    */
    'archive_batch' => env('LOGS_ARCHIVE_BATCH', 1000),
];
