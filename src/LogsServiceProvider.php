<?php

namespace Odysseycrew\Logs;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class LogsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'logs');
        $this->publishes([
            __DIR__ . '/controllers/EmptyLogController.php' => app_path('/Http/Controllers/OdysseyCrew/LogController.php'),
        ], 'odyseycrew-logs-controller');
        $this->publishes([
            __DIR__ . '/controllers/EmptyLogArchiveController.php' => app_path('/Http/Controllers/OdysseyCrew/LogArchiveController.php'),
        ], 'hermit-logs-archives-controller');
        $this->publishes([
            __DIR__ . '/views/prioritiesForm.blade.php' => resource_path('views/vendor/logs/prioritiesForm.blade.php')
        ], 'odyseycrew-logs-views');
        $this->publishes([
            __DIR__ . '/../config/' => config_path(),
        ], 'odyseycrew-logs-config');
        if ($this->app->runningInConsole()) {
            $this->commands([
                LogsInstallCommand::class,
                ArchiveLogs::class,
                LogsUpdateCommand::class,
                RedisHandleLogs::class
            ]);
        }
        if (config('logger.enabled') == true) {
            $this->app->booted(function () {
                $schedule = app(Schedule::class);
                if (config('logger.destination') == 'file') {
                    $schedule->command('logs:archive --files')->dailyAt('01:00');
                }
                if (config('logger.automatic_db_archive') == true) {
                    $schedule->command('logs:archive --automatic')->MonthlyOn('1');
                }
                if (config('logger.save_method') == 'redis') {
                    $schedule->command('logs:redis');
                }
            });
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/../routes/web.php';
        $this->app->make('Odysseycrew\Logs\MainLogController');
        $this->app->make('Odysseycrew\Logs\MainLogArchiveController');
    }
}
