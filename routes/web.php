<?php

Route::prefix('logs')->name('logs.')->middleware('web')->group(function () {
    Route::get('/', 'odysseycrew\logs\MainLogController@index')->name('index');
    Route::post('/priorities', 'odysseycrew\logs\MainLogController@savePriorities')->name('savePriorities');
    Route::prefix('archives')->name('archives.')->group(function () {
        Route::get('/', 'odysseycrew\logs\MainLogArchiveController@index')->name('index');
        Route::get('/details/{logArchive}', 'odysseycrew\logs\MainLogArchiveController@details')->name('details');
    });
});
