<!DOCTYPE html>
<html>
<head>
    <title>Hermit Logs</title>
    <style>
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        .log-priority-form {
            width: 255px;
        }
    </style>
</head>
<body>
<div class="container">
    <a href="{{route('logs.index')}}">BACK</a>
    <hr>
    <div class="content">
        <table border="1px" style="width: 100%;">
            <tr>
                <th style="width: 3%">ID</th>
                <th style="width: 20%">FROM</th>
                <th style="width: 20%">TO</th>
                <th style="width: 33%">COMMENT</th>
                <th style="width: 8%">DATE</th>
                <th style="width: 16%"></th>
            </tr>
            @foreach($logArchives as $logArchive)
                <tr>
                    <td>{{$logArchive->id}}</td>
                    <td>{{$logArchive->from}}</td>
                    <td>{{$logArchive->to}}</td>
                    <td>{{$logArchive->comment}}</td>
                    <td>{{$logArchive->created_at}}</td>
                    <td><a href="{{route('logs.archives.details',['logArchive'=>$logArchive->id])}}"><button>DETAILS</button></a></td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
</body>
</html>
